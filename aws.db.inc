<?php

/*
 * @file import_aws/aws.db.inc
 */
function _aws_get_field_mappings($qid) {
  $query = "SELECT fr.cck_field_name AS field_name, fr.aws_attribute_xpath AS xpath ".
    "FROM {aws_cck_field_references} fr ".
    "WHERE fr.tid = %d";
  
  $results = db_query($query, $qid);
  while ($result = db_fetch_array($results)) {
    $output[$result['field_name']] = $result['xpath'];
  }
  return $output;
}


function _aws_get_query_details() {
  $qid = arg(5);
  $query = "SELECT query_name, param_name, param_value FROM {aws_queries} WHERE qid = %d";
  $results = db_fetch_array(db_query($query, $qid));
  $params = unserialize($results['param_name']);
  $values = unserialize($results['param_value']);
  
  foreach ($params as $k => $v) {
    $div_id = str_replace(" ", "_", $results['query_name'] ."-". $v ."-". $values[$k]);
    $output[] = "<div id='". $div_id ."-filter' class='active_search_param'>" .
                "Searching on: <span class='param_name'>". $v ."</span>" .
                " for: <span class='param_value'>". $values[$k] ."</span>" .
                " <span class='aws_actions'><a href='#' onclick='javascript:aws_delete_parameter(this)'>(delete)</a></span>" .
                "</div>";
  }
  $output = implode(">>><<<", $output);
  print $output;
}


function _aws_get_query($qid) {
  $query = "SELECT q.query_name AS query_name,
                    q.qid,
                    q.fid,
                    ta.aws_search_index AS search_index,
                    ta.cck_type_name AS type_name
                    FROM {aws_queries} q
                    LEFT JOIN aws_cck_type_association ta ON ta.fid=q.fid
                    WHERE q.qid = %s";
  $results = db_fetch_array(db_query($query, $qid));
  return $results;
}

function _aws_get_query_list() {
    $query = "SELECT q.query_name AS query_name,
                    q.qid,
                    ta.aws_search_index AS search_index,
                    ta.cck_type_name AS type_name
                    FROM {aws_queries} q
                    LEFT JOIN aws_cck_type_association ta ON ta.fid=q.fid";
  $results = db_query($query);
  while ($result = db_fetch_array($results)) {
       $output[] = array(l($result['query_name'], "admin/settings/aws/query/" . $result['qid']), $result['search_index'], $result['type_name'], $result['qid']);
    }

  return $output;
}

/**
 * Helper: make sure new query's name is unique
*/
function _aws_is_unique_query_name($query_name) {
  return db_result(db_query('SELECT query_name from {aws_queries} WHERE query_name=\'%s\'', $query_name)) ? FALSE : TRUE;
  return $r;
}


/**
 * Save a new query.
 *
 * @param array $query - the query to save. Keys: query_name, param_name, param_value, index.
 * @return bool true on successful insertion
*/
function _aws_query_save($query) {
  //return TRUE;
  if (_aws_is_unique_query_name($query['query_name']) == TRUE) {
    $id = db_next_id("aws_queries");
    $dbquery = 'INSERT INTO {aws_queries} (query_name, fid, qid) VALUES (\'%s\', \'%s\', \'%s\')';
    if (db_query($dbquery, $query['query_name'], $query['content_mapping'], $id)) {
      return $id;
    }
  }
  else {
    
    $query1 = 'SELECT param_name, param_value FROM {aws_queries} WHERE query_name = \'%s\'';
    $result = db_fetch_array(db_query($query1, $query['query_name']));
    if ($result['param_name'] == "") {
      $params[] = $query['param_name'];
      $values[] = $query['param_value'];
      $params = serialize($params);
      $values = serialize($values);
    }
    else {
      $params = unserialize($result['param_name']);
      $values = unserialize($result['param_value']);
      $params[] = $query['param_name'];
      $values[] = $query['param_value'];
      $params = serialize($params);
      $values = serialize($values);
    }
    
    $dbquery = 'UPDATE {aws_queries} SET param_name = \'%s\', param_value = \'%s\' WHERE query_name = \'%s\'';
    return db_query($dbquery, $params, $values, $query['query_name']);
  }
  return FALSE;
}

function _aws_query_del($query) {
  $qid = $query['qid'];
  $dbquery = "SELECT query_name, param_name, param_value FROM {aws_queries} WHERE qid = %d";
  $results = db_fetch_array(db_query($dbquery, $qid));
  $params = unserialize($results['param_name']);
  $values = unserialize($results['param_value']);
  foreach ($values as $k => $v) {
    if ($v == $query['param_value'] && $params[$k] == $query['param_name']) {
      unset($values[$k]);
      unset($params[$k]);
      $success = TRUE;
    }
  }
  if ($success) {
    $params = serialize($params);
    $values = serialize($values);      
    $dbquery = 'UPDATE {aws_queries} SET param_name = \'%s\', param_value = \'%s\' WHERE query_name = \'%s\'';
    return db_query($dbquery, $params, $values, $query['query_name']);
  }
  return FALSE;
}

function _aws_get_association($fid) {
  $query = "SELECT cck_type_name, aws_search_index, description FROM {aws_cck_type_association} WHERE fid = %d";
  $result = db_fetch_array(db_query($query, $fid));
  return $result;
}


/**
 * Load a query UNFINISHED
*/
function _aws_query_load($which_query = NULL) {
  if (!$which_query) {
    $dbquery = 'INSERT INTO {aws_queries} (query_name, search_index) VALUES (\'%s\', \'%s\')';
  }
  $dbquery = 'INSERT INTO {aws_queries} (query_name, search_index) VALUES (\'%s\', \'%s\')';
  db_query($dbquery, $query['query_name'], $query['search_index']);
}

function _aws_get_content_mappings() {
  $mappings = array();
  $resultset = db_query('SELECT fid, cck_type_name, aws_search_index, description FROM {aws_cck_type_association}');
  while ($result = db_fetch_array($resultset)) {
    $mappings[$result['description'] ." (". $result['cck_type_name'] .", ID# ". $result['fid'] .")"] = array($result['fid'] => $result['aws_search_index']);
  }
  return $mappings;
}


function _aws_get_search_indices() {
  static $indices;
  if ($indices) {
    return $indices;
  }
  $indices = array();
  $query = "SELECT DISTINCT(index_name) as index_name from {aws_search_indices} order by index_name";
  $resultset = db_query($query);
  while ($row = db_fetch_array($resultset)) {
    $indices[] = $row['index_name'];
  }
  return $indices;
}


function aws_get_search_params($index_name) {
  $params = array();
  $resultset = db_query('SELECT param_name FROM {aws_search_indices} WHERE index_name=\'%s\'', $index_name);
  while ($row = db_fetch_array($resultset)) {
    $params[] = $row['param_name'];
  }
  return $params;
}


function _aws_process_index($index, &$results) {
  $index_name = $index->name;
  $msg = '<b>Processing search index '. $index_name .'</b>';
  $results[] = $msg;
  watchdog('info', $msg);
  $existing_params = aws_get_search_params($index_name);
  $insert_query = 'INSERT INTO {aws_search_indices} (index_name, param_name) VALUES(\'%s\', \'%s\')';
  
  // add new params to the table
  $params = $index->xpath('params/param');
  if (!is_array($params)) {
    $params = array($params);
  }
  // multiple params for search index
  foreach ($params as $param) {
    if (!in_array($param, $existing_params)) {
      $msg = 'processing param '. $param;
      $results[] = $msg;
      watchdog('info', $msg);
      db_query($insert_query, $index_name, $param);
    } 
    else {
      $msg = 'Skipping param '. $param;
      $results[] = $msg;
      watchdog('info', $msg);
    }
  }
  
  // delete params that are no longer valid
  $deleted_params = array_diff($existing_params, $params);
  foreach ($deleted_params as $deleted_param) {
    
    db_query('DELETE FROM {aws_search_indices} WHERE index_name=\'%s\' AND param_name=\'%s\'', $index_name, $deleted_param);
    $msg = 'Deleting param '. $deleted_param .' because it no longer exists.';
    $results[] = $msg;
    watchdog('info', $msg);
  }
  return $msg;
}

<?php


define('AWS_IMPORT_LIBRARY', '1');
define('REQUEST_URL', 'http://ecs.amazonaws.com/onca/xml?Service=AWSECommerceService&AWSAccessKeyId=%s&AssociateTag=%s&Version=2006-09-11&Operation=ItemSearch&ResponseGroup=Large,Offers&SearchIndex=%s&%s&ItemPage=%s');
define('LOOKUP_REQUEST_URL', 'http://ecs.amazonaws.com/onca/xml?Service=AWSECommerceService&AWSAccessKeyId=%s&AssociateTag=%s&Version=2006-09-11&Operation=ItemLookup&ResponseGroup=Large,Offers&ItemId=%s');

class AWS_Rest_Request {
    private $action = 'Search';
    private $itemPage = '1';
    private $accessKeyId;
    private $secretAccessKey;
    private $associateTag = "workhabt";
    
    public function __construct($accessKeyId, $secretAccessKey) {
        $this->accessKeyId = $accessKeyId;
        $this->secretAccessKey = $secretAccessKey;
    }

    public function getItem($asin) {
      $request_url = sprintf(LOOKUP_REQUEST_URL, $this->accessKeyId, $this->associatetag, $asin);
      $result = $this->_do_curl_request($request_url);
      // hack to work around php 5.0's problems with default namespaces and xpath()
      $result = str_replace("xmlns=","a=",$result);
      $xml = simplexml_load_string($result);
      
      return $xml;
    }

    private function _do_curl_request($request_url) {
      $ch = curl_init($request_url);
      curl_setopt($ch, CURLOPT_HEADER, FALSE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      $result = curl_exec($ch);
      curl_close($ch);
      return $result;
    }
    public function search($search_index, $keywords, $itemPage = 1) {
        $request_url = sprintf(REQUEST_URL, $this->accessKeyId, $this->associateTag, $search_index, $keywords, $itemPage);
        $ch = curl_init($request_url);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($ch);
        curl_close($ch);
        
        // hack to work around php 5.0's problems with default namespaces and xpath()
        $result = str_replace("xmlns=","a=",$result);
        $xml = simplexml_load_string($result);

        return $xml;
    }
    
    public function aggregateItemAttributes(&$xml_result) {
        $items = $xml_result->Items->Item;
        $retobj = array();
        $vartree = array();
        foreach ($items as $item) {
            $this->_traverse_vars(get_object_vars($item), $vartree);
        }
        return $vartree;
    }
    
    public function _traverse_vars(&$vars, &$vartree, $current_var_name = '') {
        $var_names = array_keys($vars);
        foreach ($var_names as $var_name) {
            if ($vars[$var_name] instanceof SimpleXMLElement) {
                $this->_traverse_vars(get_object_vars($vars[$var_name]), $vartree, $current_var_name . $var_name . '/');
            } else {
                $vartree[$current_var_name . $var_name] = $current_var_name . $var_name;
            }
        }
    }
}

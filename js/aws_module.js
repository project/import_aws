var aws = new Object;
aws.init = function() {
  aws_ajax_status_indicator("on", "Loading parameters. . . ");
  var sQID = $('input#edit-qid').val();
  try {
    $.ajax({ async: true, 
              url: '/admin/settings/aws/ajax/getquery/' + sQID, 
              dataType: 'html',
              success: aws_load_query });
  } catch(ex) {
  }
}

$(aws.init);



function aws_ajax_import(oObj) {
  //if(oObj.checked){
    var sString;
    aQuery = new Array();
    $(oObj).parent().parent().find('td').each(function(){
      $(this).effect("highlight", { color: "#458D45" }, 5000);
      bIsAttribute = $(this).attr("class").indexOf("marker_");
      if(bIsAttribute >= 0) {
        sImgException = $(this).children("img").attr("src");
        if(typeof(sImgException) == "undefined" || sImgException == "undefined") {
          sValue = $(this).html();
        }
        else {
          sValue = sImgException;
        }
        aQuery.push($(this).attr("class").substr(bIsAttribute + 7) + "=" + sValue);
      }
    });
    nRandom = Math.floor(Math.random() * 10000);
    aQuery.push("cck_type=" + $('input#edit-type-name').val());
    aQuery.push("random_number=" + nRandom);
    sString = aQuery.join("&");
    
    $(oObj).parent().html("<span class='import-status-saving' id='" + nRandom + "'>Saving. . . </span>");
    _aws_save_node(sString);
}

function _aws_save_node(sQuery) {
   try {
    $.ajax({ async: true, 
                        type: 'POST', 
                        url: '/admin/settings/aws/ajax/node_save', 
                        dataType: 'html',
                        data: sQuery,
                        success: _aws_node_saved });
  } catch(ex) {
  }
  return false;
}


function _aws_node_saved(data, textStatus) {
  var sResults = data.split(",");
  $("#" + sResults[1]).parent().html("<span class='aws_saved'>Saved as <a href='/node/" + sResults[0] + "' target='blank'>Node " + sResults[0] + "</a></span");
  return false;
}




function aws_load_query(data, textStatus) {
  var aQueries = data.split(">>><<<");

  for(var i in aQueries) {
    aws_ajax_status_indicator("on", "Loading query. . .");
    aws_filter_add_ajax_saved(aQueries[i]);
  }
  aws_ajax_status_indicator("off", null);
}



function aws_filter_add_ajax(oObj) {
  aws_ajax_status_indicator("on", "Saving parameter...");
  $("input#add_filter").disabled(true);
  var aFields = new Array();
  try {
    var sData = 'param_name=' + escape($('select#edit-param-name').val()) +
        '&param_value=' + escape($('input#edit-param-value').val()) +
        '&index=' + escape($('input#edit-search-index').val()) +
        '&query_name=' + escape($('input#edit-query-name').val());
        
    $.ajax({ async: true, 
                        type: 'POST', 
                        url: '/admin/settings/aws/ajax/add_param', 
                        dataType: 'html',
                        data: sData,
                        success: aws_filter_add_ajax_saved });
  } catch(ex) {
  }

  return false;
}


function aws_refresh_view() {
  aQuery = new Array();
  $('#aws-query-run-build-form div fieldset').find('div.active_search_param').each(
                                    function(){
                                      aQuery.push($(this).find('span.param_name').html() + "=" + $(this).find('span.param_value').html());
                                    });
  sQuery = "index=" + $('input#edit-search-index').val() + "&" + aQuery.join("&") + "&qid=" + $('input#edit-qid').val() + "&fid=" + $('input#edit-fid').val();
    
  //alert(sQuery);
  $("input#add_filter").disabled(true);
  $("input#refresh_view").disabled(true);
  aws_ajax_status_indicator("on", "Refreshing view. . .");
  
  aws_execute_query(sQuery);
  return false;
}

function aws_delete_parameter(oWhich) {
  $(oWhich).parent().parent().each(
                                    function(){
                                      sParamName = $(this).find('span.param_name').html();
                                      sParamValue = $(this).find('span.param_value').html();
                                    });
  sQuery = "index=" + $('input#edit-search-index').val() + "&param_name=" + sParamName + "&param_value=" + sParamValue + "&query_name=" + $('input#edit-query-name').val() + "&qid=" + $('input#edit-qid').val();
  $(oWhich).parent().html("<span class='delete_status'>(Stand by...)</span>");
    
  aws_delete_param_helper(sQuery);
  return false;
}


function aws_delete_param_helper(sQuery) {
  $("input#add_filter").disabled(true);
  $("input#refresh_view").disabled(true);
  aws_ajax_status_indicator("on", "Working. . .");
  
  try {
        
    $.ajax({ async: true, 
                        type: 'POST', 
                        url: '/admin/settings/aws/ajax/del_param', 
                        dataType: 'html',
                        data: sQuery,
                        success: aws_filter_del_ajax_saved });
  } catch(ex) {
  }

  return false;
}


function aws_filter_del_ajax_saved(data, textStatus) {
  $("input#add_filter").disabled(false);
  $("input#refresh_view").disabled(false);
  aws_ajax_status_indicator("off", null);
  $('#' + data + "-filter").effect("highlight", { color: "#C66464" }, 500);
  $('#' + data + "-filter").slideUp("slow", function(){
    $('#' + data + "-filter").remove();
  });
  
  aws_refresh_pager_view();
}


function aws_refresh_pager_view(sPage, sDirection) {
  sDir = sDirection; 
  aQuery = new Array();
  $('#aws-query-run-build-form div fieldset').find('div.active_search_param').each(
                                    function(){
                                      aQuery.push($(this).find('span.param_name').html() + "=" + $(this).find('span.param_value').html());
                                    });
  sQuery = "index=" + $('input#edit-search-index').val() + "&" + aQuery.join("&") + "&qid=" + $('input#edit-qid').val() + "&fid=" + $('input#edit-fid').val();
  sQuery += "&page=" + sPage;
  
  $("input#add_filter").disabled(true);
  $("input#refresh_view").disabled(true);
  aws_ajax_status_indicator("on", "Refreshing view. . .");
  
  aws_execute_query(sQuery);
  return false;
}

function aws_filter_add_ajax_saved(data, textStatus) {
  $('form#aws-query-run-build-form div fieldset').find('div.form-item:first').each(
    function(){
      $(this).prepend(data);
      $(this).find('div:first').each(
                                    function(){
                                      $(this).effect("highlight", { color: "#458D45" }, 2000);
                                      $("input#add_filter").disabled(false);
                                    });                                                                                   
  });
  if(textStatus) {
    aws_ajax_status_indicator("off", null);
  }
  return false;
}

function aws_execute_query(sQueryString) {   
  try {
    $.ajax({ async: true, 
                        type: 'POST', 
                        url: '/admin/settings/aws/ajax/doquery', 
                        dataType: 'html',
                        data: sQueryString,
                        success: aws_query_executed });
  } catch(ex) {
  }
  return false;
}


function aws_query_executed(data, textStatus) {
  if(typeof(sDir) == "undefined") {
    sDir = "normal";
  }
  switch(sDir) {
    case "normal":
      $('#aws_resultset').slideUp();  
      $('#aws_resultset').html(data);
      $('#aws_resultset').slideDown();
      $("input#add_filter").disabled(false);
      $("input#refresh_view").disabled(false);
      aws_ajax_status_indicator("off", null);
      break;
    
    case "gt":
      $('#aws_resultset').hide("slide", { direction: "left" }, 500);
      $('#aws_resultset').html(data);
      $('#aws_resultset').show("slide", { direction: "right" }, 500);
      $("input#add_filter").disabled(false);
      $("input#refresh_view").disabled(false);
      aws_ajax_status_indicator("off", null); 
      break;
    
    case "lt":
      $('#aws_resultset').hide("slide", { direction: "right" }, 500);
      $('#aws_resultset').html(data);
      $('#aws_resultset').show("slide", { direction: "left" }, 500);
      $("input#add_filter").disabled(false);
      $("input#refresh_view").disabled(false);
      aws_ajax_status_indicator("off", null); 
      break;  
  }
  
}




function aws_ajax_status_indicator(sState, sText) {
  switch(sState) {
    case "off":
      $('#aws_status_throbber').stop();
      $('#aws_status_throbber').hide();
      $('#aws_status_throbber').html('');
      break;
    case "on":
      $('#aws_status_throbber').html(sText);
      $('#aws_status_throbber').show();
      $('#aws_status_throbber').effect("pulsate", { times: 20 }, 1000);
      break;
  }
}


jQuery.fn.extend({
filterDisabled : function(){ return this.filter(function(){return (typeof(this.disabled)!=undefined)})},
disabled: function(h) {
   if (h!=undefined) return this.filterDisabled().each(function(){this.disabled=h});
   this.filterDisabled().each(function() {h=((h||this.disabled)&&this.disabled)}); return h;
},
toggleDisabled: function() { return this.filterDisabled().each(function(){this.disabled=!this.disabled});}
});